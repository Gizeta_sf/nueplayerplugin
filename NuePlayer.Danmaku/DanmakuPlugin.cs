﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Gizeta.Macula.ComponentModel;
using Gizeta.Macula.Controls;
using Gizeta.Macula.Plugin.Player;
using Gizeta.Macula.Utility;
using Gizeta.NuePlayer.Danmaku.CommentsData;
using Gizeta.NuePlayer.Danmaku.ComponentModel;
using Gizeta.NuePlayer.Danmaku.Controls;
using Gizeta.NuePlayer.Danmaku.DataParser;
using Gizeta.NuePlayer.Danmaku.Utility;

namespace Gizeta.NuePlayer.Danmaku
{
    public class DanmakuPlugin : IPlayerPlugin
    {
        private Canvas dmCanvas;
        private StackPanel dmPanel;
        private TabItem dmListTabItem;
        private CommentDataGridControl dmDataGrid;
        
        public bool IsRunOnce
        {
            get { return false; }
        }

        public IPluginInfo PluginInfo
        {
            get { return new DanmakuPluginInfo(); }
        }

        public void Execute(Dictionary<string, string> param)
        {
            if (!param.ContainsKey("ctype")) return;

            PlayerMediator.Instance.NotificationPanel.AddMessage("开始载入弹幕");

            IDataParser parser = DataParserFactory.GetDataParser(param["ctype"]);
            if (parser != null)
            {
                parser.ParseCompleted += parser_ParseCompleted;
                parser.Erred += parser_Erred;
                parser.Load(param);
            }
            else
            {
                PlayerMediator.Instance.NotificationPanel.AddMessage("弹幕解析失败:未找到对应的解析工具");
            }
        }

        public void Initialize(bool isFirstRun = false)
        {
            if (isFirstRun)
            {
                IPlayerComponent playerContainer = PlayerMediator.Instance.PlayerContainer;
                IControlComponent controlBar = PlayerMediator.Instance.ControlBar;
                IConfigComponent configPage = PlayerMediator.Instance.ConfigPage;

                if (playerContainer.FindName("Danmaku_Canvas") != null)
                {
                    dmCanvas = playerContainer.FindName("Danmaku_Canvas") as Canvas;
                }
                else
                {
                    dmCanvas = new Canvas();
                    dmCanvas.Name = "Danmaku_Canvas";
                    (playerContainer.FindName("LayoutRoot") as Grid).Children.Add(dmCanvas);
                }
                CommentProvider.Instance.Manager = new CommentManager(dmCanvas);

                PlayerMediator.Instance.MainPage.ChangeControlBarOriginalHeight(43);
                Slider timeSlider = controlBar.FindName("TimeSlider") as Slider;
                timeSlider.SetValue(Grid.ColumnProperty, 0);
                timeSlider.SetValue(Grid.ColumnSpanProperty, 3);
                timeSlider.SetValue(Grid.RowProperty, 0);
                dmPanel = controlBar.FindName("MiddleControlPanel") as StackPanel;
                dmPanel.Orientation = Orientation.Vertical;

                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("/NuePlayer.Danmaku;Component/Controls/ControlResourceDictionary.xaml", UriKind.Relative)
                });
                Grid grid1 = new Grid()
                {
                    ColumnDefinitions =
                    {
                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
                        new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) }
                    },
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(4,0,4,0)
                };
                TextBlock tbl1 = new TextBlock()
                {
                    Text = "Mode:",
                    Style = Application.Current.Resources["CommentSenderTextBlockStyle"] as Style
                };
                Grid.SetColumn(tbl1, 0);
                grid1.Children.Add(tbl1);
                TextBox tbx1 = new TextBox()
                {
                    Text = "1",
                    Style = Application.Current.Resources["CommentSenderTextBoxStyle"] as Style
                };
                Grid.SetColumn(tbx1, 1);
                grid1.Children.Add(tbx1);
                TextBlock tbl2 = new TextBlock()
                {
                    Text = "Size:",
                    Style = Application.Current.Resources["CommentSenderTextBlockStyle"] as Style
                };
                Grid.SetColumn(tbl2, 2);
                grid1.Children.Add(tbl2);
                TextBox tbx2 = new TextBox()
                {
                    Text = "25",
                    Style = Application.Current.Resources["CommentSenderTextBoxStyle"] as Style
                };
                Grid.SetColumn(tbx2, 3);
                grid1.Children.Add(tbx2);
                TextBlock tbl3 = new TextBlock()
                {
                    Text = "Color:",
                    Style = Application.Current.Resources["CommentSenderTextBlockStyle"] as Style
                };
                Grid.SetColumn(tbl3, 4);
                grid1.Children.Add(tbl3);
                TextBox tbx3 = new TextBox()
                {
                    Text = "#FFFFFF",
                    Style = Application.Current.Resources["CommentSenderTextBoxStyle"] as Style
                };
                Grid.SetColumn(tbx3, 5);
                grid1.Children.Add(tbx3);
                TextBlock tbl4 = new TextBlock()
                {
                    Text = "Text:",
                    Style = Application.Current.Resources["CommentSenderTextBlockStyle"] as Style
                };
                Grid.SetColumn(tbl4, 6);
                grid1.Children.Add(tbl4);
                TextBox tbx4 = new TextBox()
                {
                    Text = "",
                    Style = Application.Current.Resources["CommentSenderTextBoxStyle"] as Style
                };
                Grid.SetColumn(tbx4, 7);
                grid1.Children.Add(tbx4);
                Button btn1 = new Button()
                {
                    Content =
                        new Grid()
                        {
                            Children = { new TextBlock() { Text = "Send" } },
                            Background = new SolidColorBrush(Color.FromArgb(255, 136, 136, 136)),
                            Height = 20,
                            Margin = new Thickness(-1, -1, -1, 0),
                            VerticalAlignment = VerticalAlignment.Center
                        },
                    Foreground = new SolidColorBrush(Colors.Black)
                };
                btn1.Click +=
                    (sender, args) =>
                    {
                        CommentSender.Instance.Send(new Dictionary<string, string>()
                        {
                            { "text", tbx4.Text },
                            {
                                "color",
                                ((int.Parse(tbx3.Text.Substring(1, 2), NumberStyles.HexNumber) <<
                                  16) + (int.Parse(tbx3.Text.Substring(3, 2), NumberStyles.HexNumber) <<
                                         8) + int.Parse(tbx3.Text.Substring(5, 2), NumberStyles.HexNumber)).ToString()
                            },
                            { "fontsize", tbx2.Text },
                            { "mode", tbx1.Text },
                            { "playtime", playerContainer.Player.Position.TotalSeconds.ToString() }
                        });

                        Dictionary<string, object> data = new Dictionary<string, object>();
                        data["text"] = tbx4.Text.Replace("\\n", "\r\n");
                        data["playTime"] = playerContainer.Player.Position.TotalSeconds;
                        switch (tbx1.Text)
                        {
                            case "1":
                                data["mode"] = CommentMode.Scroll;
                                break;
                            case "4":
                                data["mode"] = CommentMode.Bottom;
                                break;
                            case "5":
                                data["mode"] = CommentMode.Top;
                                break;
                            case "6":
                                data["mode"] = CommentMode.ReverseScroll;
                                break;
                        }
                        data["lifeTime"] = 3.0;
                        data["fontsize"] = double.Parse(tbx2.Text);
                        data["color"] = (int.Parse(tbx3.Text.Substring(1, 2), NumberStyles.HexNumber) <<
                                         16) + (int.Parse(tbx3.Text.Substring(3, 2), NumberStyles.HexNumber) <<
                                                8) + int.Parse(tbx3.Text.Substring(5, 2), NumberStyles.HexNumber);
                        CommentProvider.Instance.Manager.ShowComment(new BasicCommentData(data));
                        CommentProvider.Instance.Add(new BasicCommentData(data));
                        dmDataGrid.BindingCommentList();
                    };
                Grid.SetColumn(btn1, 8);
                grid1.Children.Add(btn1);
                dmPanel.Children.Add(grid1);

                dmDataGrid = new CommentDataGridControl();
                dmDataGrid.CommentDataGrid.Style = Application.Current.Resources["ConfigComponentDataGridStyle"] as Style;
                dmListTabItem = new TabItem() { Header = "评论列表", Content = dmDataGrid, Style = Application.Current.Resources["ConfigComponentTabItemStyle"] as Style };
                (configPage.FindName("ConfigTabControl") as TabControl).Items.Insert(0, dmListTabItem);
            }
        }

        public void Unload()
        {
            IPlayerComponent playerContainer = PlayerMediator.Instance.PlayerContainer;
            IControlComponent controlBar = PlayerMediator.Instance.ControlBar;
            IConfigComponent configPage = PlayerMediator.Instance.ConfigPage;

            (playerContainer.FindName("LayoutRoot") as Grid).Children.Remove(dmCanvas);

            PlayerMediator.Instance.MainPage.ChangeControlBarOriginalHeight(33);
            Slider timeSlider = controlBar.FindName("TimeSlider") as Slider;
            timeSlider.SetValue(Grid.ColumnProperty, 1);
            timeSlider.SetValue(Grid.ColumnSpanProperty, 1);
            timeSlider.SetValue(Grid.RowProperty, 1);
            (controlBar.FindName("MiddleControlPanel") as StackPanel).Children.Remove(dmPanel);

            (configPage.FindName("ConfigTabControl") as TabControl).Items.Remove(dmListTabItem);
        }

        private void parser_Erred(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.ExceptionObject.Message);
            Logger.Log(e.ExceptionObject);
            e.Handled = true;
        }

        private void parser_ParseCompleted(object sender, EventArgs e)
        {
            PlayerMediator.Instance.NotificationPanel.AddMessage("弹幕解析完成");
            dmDataGrid.BindingCommentList();
        }
    }
}
