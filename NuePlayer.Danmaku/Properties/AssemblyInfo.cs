﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的常规信息通过以下
// 特性集控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("Danmaku Plugin for NuePlayer")]
[assembly: AssemblyDescription("NuePlayer弹幕插件")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Gizeta.NuePlayer.Danmaku")]
[assembly: AssemblyCopyright("Copyright © 2012-2013 Gizeta. All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// 将 ComVisible 设置为 false 可使此程序集中的类型
// 对 COM 组件不可见。如果需要从 COM 访问此程序集中的类型，
// 则将该类型上的 ComVisible 特性设置为 true。
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则以下 GUID 用作类型库的 ID
[assembly: Guid("db3a0819-9282-460a-8305-9e5f8fd81776")]

// 程序集的版本信息由下面四个值组成:
//
//      主版本
//      次版本
//      生成号
//      修订号
//
// 您可以指定所有这些值，也可以使用“修订号”和“生成号”的默认值，
// 方法是按如下所示使用“*”:
[assembly: AssemblyVersion("0.3.0.0")]
[assembly: AssemblyInformationalVersion("0.3.0.18")]
