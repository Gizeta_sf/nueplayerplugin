﻿using System;

using Gizeta.Macula.Plugin;
using Gizeta.Macula.Plugin.Player;

namespace Gizeta.NuePlayer.Danmaku
{
    public class DanmakuPluginInfo : IPlayerPluginInfo
    {
        public string Description
        {
            get { return "NuePlayer弹幕插件"; }
        }

        public string Author
        {
            get { return "Gizeta"; }
        }

        public string FullName
        {
            get { return "Gizeta.NuePlayer.Danmaku.DanmakuPlugin"; }
        }

        public Type InstanceType
        {
            get { return typeof(DanmakuPlugin); }
        }

        public string Name
        {
            get { return "NuePlayer.Danmaku"; }
        }

        public PluginType Type
        {
            get { return PluginType.PlayerPlugin; }
        }

        public string Version
        {
            get { return "0.2.0.16"; }
        }

        public string Website
        {
            get { return ""; }
        }
    }
}
