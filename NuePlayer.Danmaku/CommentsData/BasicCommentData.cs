﻿using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace Gizeta.NuePlayer.Danmaku.CommentsData
{
    /// <summary>
    /// 定义普通弹幕数据所实现的属性。
    /// </summary>
    public class BasicCommentData : UserCommentData
    {
        protected Color color = Colors.White;
        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        protected double fontSize = 25;
        public double FontSize
        {
            get { return fontSize; }
            set { fontSize = value; }
        }

        public BasicCommentData()
            : base()
        {
        }

        public BasicCommentData(Dictionary<string, object> data)
            : base(data)
        {
            object dictValue;
            if (data.TryGetValue("color", out dictValue))
            {
                int temp = (int)dictValue;
                color = Color.FromArgb(255, Convert.ToByte(temp >> 16), Convert.ToByte((temp >> 8) & 0xFF), Convert.ToByte(temp & 0xFF));
            }
            if (data.TryGetValue("fontsize", out dictValue))
            {
                fontSize = (double)dictValue;
            }
        }
    }
}
