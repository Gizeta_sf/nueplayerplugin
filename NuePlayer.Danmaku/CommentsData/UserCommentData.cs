﻿using System;
using System.Collections.Generic;

using Gizeta.NuePlayer.Danmaku.ComponentModel;

namespace Gizeta.NuePlayer.Danmaku.CommentsData
{
    /// <summary>
    /// 定义用户弹幕数据所实现的属性。
    /// </summary>
    public class UserCommentData : CommentData
    {
        protected uint id = 0;
        public uint ID
        {
            get { return id; }
        }

        protected bool isLocked = false;
        public bool IsLocked
        {
            get { return isLocked; }
        }

        protected CommentMode mode = CommentMode.Scroll;
        public CommentMode Mode
        {
            get { return mode; }
            set { mode = value; }
        }

        protected string text = "";
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        protected DateTime timestamp = DateTime.MinValue;
        public DateTime Timestamp
        {
            get { return timestamp; }
        }

        protected string userID = "";
        public string UserID
        {
            get { return userID; }
        }

        public UserCommentData()
            : base()
        {
        }

        public UserCommentData(Dictionary<string, object> data)
            : base(data)
        {
            object dictValue;
            if (data.TryGetValue("id", out dictValue))
            {
                id = (uint)dictValue;
            }
            if (data.TryGetValue("lock", out dictValue))
            {
                isLocked = (bool)dictValue;
            }
            if (data.TryGetValue("mode", out dictValue))
            {
                mode = (CommentMode)dictValue;
            }
            if (data.TryGetValue("text", out dictValue))
            {
                text = (string)dictValue;
            }
            if (data.TryGetValue("timestamp", out dictValue))
            {
                long temp = (long)dictValue;
                timestamp = DateTime.FromFileTime(temp);
            }
            if (data.TryGetValue("userID", out dictValue))
            {
                userID = (string)dictValue;
            }
        }
    }
}
