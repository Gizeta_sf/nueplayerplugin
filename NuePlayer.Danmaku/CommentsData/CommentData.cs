﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Gizeta.NuePlayer.Danmaku.CommentsData
{
    /// <summary>
    /// 定义弹幕数据所实现的属性。
    /// </summary>
    public class CommentData
    {
        protected bool isPlaying = false;
        public bool IsPlaying
        {
            get { return isPlaying; }
        }
        
        protected Duration lifeTime = Duration.Forever;
        public Duration LifeTime
        {
            get { return lifeTime; }
            set { lifeTime = value; }
        }

        protected TimeSpan playTime = TimeSpan.Zero;
        public TimeSpan PlayTime
        {
            get { return playTime; }
            set { playTime = value; }
        }

        public CommentData()
        {
        }

        public CommentData(Dictionary<string, object> data)
        {
            object dictValue;
            if (data.TryGetValue("lifeTime", out dictValue))
            {
                lifeTime = TimeSpan.FromSeconds((double)dictValue);
            }
            if (data.TryGetValue("playTime", out dictValue))
            {
                playTime = TimeSpan.FromSeconds((double)dictValue);
            }
        }
    }
}
