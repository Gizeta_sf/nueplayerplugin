﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

using Gizeta.NuePlayer.Danmaku.Comments;
using Gizeta.NuePlayer.Danmaku.ComponentModel;
using System;
using System.Windows.Media;

namespace Gizeta.NuePlayer.Danmaku.Utility
{
    /// <summary>
    /// 弹幕空间管理者
    /// <para>仅对普通弹幕进行空间管理</para>
    /// </summary>
    public class CommentSpaceManager
    {
        private Canvas canvas = null;
        private List<List<BasicComment>> dmPools = new List<List<BasicComment>>() { new List<BasicComment>(), new List<BasicComment>() };
        
        public CommentSpaceManager(Canvas canvas)
        {
            this.canvas = canvas;
        }

        public void AddComment(BasicComment cmt)
        {
            cmt.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            cmt.SetMeasureSize();
            switch (cmt.Mode)
            {
                case CommentMode.Scroll:
                    AllocateScrollComment(cmt);
                    break;
                case CommentMode.ReverseScroll:
                    AllocateReverseScrollComment(cmt);
                    break;
                case CommentMode.Top:
                    AllocateTopComment(cmt);
                    break;
                case CommentMode.Bottom:
                    AllocateBottomComment(cmt);
                    break;
                default:
                    AllocateAdvancedComment(cmt);
                    break;
            }
            canvas.Children.Add(cmt);
        }

        public void RemoveComment(BasicComment cmt)
        {
            dmPools[cmt.LayerIndex].Remove(cmt);
            canvas.Children.Remove(cmt);
        }

        public void PauseAllComment()
        {
            foreach (var pool in dmPools)
            {
                foreach (var cmt in pool)
                {
                    cmt.Pause();
                }
            }
        }

        public void ResumeAllComment()
        {
            foreach (var pool in dmPools)
            {
                foreach (var cmt in pool)
                {
                    cmt.Resume();
                }
            }
        }

        private void AllocateTopComment(BasicComment cmt)
        {
            cmt.Speed = 0;
            cmt.X = (canvas.ActualWidth - cmt.MeasuredWidth) / 2;
            setY(cmt);
        }

        private void AllocateBottomComment(BasicComment cmt)
        {
            cmt.Speed = 0;
            cmt.X = (canvas.ActualWidth - cmt.MeasuredWidth) / 2;
            setY(cmt, true);
        }

        private void AllocateScrollComment(BasicComment cmt)
        {
            cmt.Speed = (canvas.ActualWidth + cmt.MeasuredWidth) / cmt.LifeTime.TimeSpan.TotalSeconds;
            cmt.X = canvas.ActualWidth;
            setY(cmt);
            DoubleAnimation animation = new DoubleAnimation();
            animation.From = canvas.ActualWidth;
            animation.To = -cmt.MeasuredWidth;
            animation.Duration = cmt.LifeTime;
            Storyboard.SetTarget(animation, cmt);
            Storyboard.SetTargetProperty(animation, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateX)"));
            cmt.AnimationCollection.Add(animation);
        }

        private void AllocateReverseScrollComment(BasicComment cmt)
        {
            cmt.Speed = -(canvas.ActualWidth + cmt.MeasuredWidth) / cmt.LifeTime.TimeSpan.TotalSeconds;
            cmt.X = -cmt.MeasuredWidth;
            setY(cmt);
            DoubleAnimation animation = new DoubleAnimation();
            animation.From = -cmt.MeasuredWidth;
            animation.To = canvas.ActualWidth;
            animation.Duration = cmt.LifeTime;
            Storyboard.SetTarget(animation, cmt);
            Storyboard.SetTargetProperty(animation, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateX)"));
            cmt.AnimationCollection.Add(animation);
        }

        private void setY(BasicComment cmt, bool bottom = false)
        {
//             if (cmt.Text.Contains("s2"))
//             {
//                 System.Diagnostics.Debugger.Break();
//             }
            for (int i = 1; i < dmPools.Count; i++)
            {
                var pool = dmPools[i];
                if (pool.Count == 0)
                {
                    cmt.LayerIndex = i;
                    cmt.Y = 0;
                    pool.Add(cmt);
                    return;
                }
                if (bottom)
                {   
                    double tempHeight = canvas.ActualHeight;
                    for (int j = pool.Count - 1; j >= 0; j++)
                    {
                        if (cmt.MeasuredHeight <= tempHeight - pool[j].Y)
                        {
                            cmt.LayerIndex = i;
                            cmt.Y = tempHeight;
                            pool.Insert(j, cmt);
                            return;
                        }
                        if (collisionText(cmt, pool[j]))
                        {
                            tempHeight = pool[j].X;
                            if (j == pool.Count - 1)
                            {
                                if (cmt.MeasuredHeight <= tempHeight)
                                {
                                    cmt.LayerIndex = i;
                                    cmt.Y = tempHeight;
                                    pool.Add(cmt);
                                    return;
                                }
                                break;
                            }
                        }
                        else
                        {
                            cmt.LayerIndex = i;
                            cmt.Y = tempHeight;
                            pool.Insert(j, cmt);
                            return;
                        }
                    }
                }
                else
                {
                    double tempHeight = 0;
                    for (int j = 0; j < pool.Count; j++)
                    {
                        if (cmt.MeasuredHeight <= pool[j].Y - tempHeight)
                        {
                            cmt.LayerIndex = i;
                            cmt.Y = tempHeight;
                            pool.Insert(j, cmt);
                            return;
                        }
                        if (collisionText(cmt, pool[j]))
                        {
                            tempHeight = pool[j].Bottom;
                            if (j == pool.Count - 1)
                            {
                                if (cmt.MeasuredHeight <= canvas.ActualHeight - tempHeight)
                                {
                                    cmt.LayerIndex = i;
                                    cmt.Y = tempHeight;
                                    pool.Add(cmt);
                                    return;
                                }
                                break;
                            }
                        }
                        else
                        {
                            cmt.LayerIndex = i;
                            cmt.Y = tempHeight;
                            pool.Insert(j, cmt);
                            return;
                        }
                    }
                }
            }
            dmPools.Add(new List<BasicComment>() { cmt });
            cmt.LayerIndex = dmPools.Count - 1;
            cmt.Y = 0;
        }

        private bool collisionText(BasicComment newCmt, BasicComment Cmt)
        {
            double d1 = newCmt.X - Cmt.X - Cmt.MeasuredWidth;
            double d2 = newCmt.X + Cmt.MeasuredWidth - Cmt.X;
            if (d1 * d2 <= 0)
            {
                return true;
            }
            if (newCmt.Speed == Cmt.Speed)
            {
                return false;
            }
            double restTime = Math.Min((newCmt.LifeTime.TimeSpan - newCmt.CurrentTime).TotalSeconds, (Cmt.LifeTime.TimeSpan - Cmt.CurrentTime).TotalSeconds);
            double distance = d1 > 0 ? Math.Min(d1, d2) : Math.Max(d1, d2);
            double collisionTime = distance / (newCmt.Speed - Cmt.Speed);
            if (collisionTime >= 0 && collisionTime < restTime)
            {
                return true;
            }
            return false;
        }

        private void AllocateAdvancedComment(BasicComment cmt)
        {
            cmt.LayerIndex = 0;
            dmPools[0].Add(cmt);
        }
    }
}
