﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

using Gizeta.Macula.ComponentModel;
using Gizeta.Macula.Controls;
using Gizeta.Macula.Utility;
using Gizeta.NuePlayer.Danmaku.Comments;
using Gizeta.NuePlayer.Danmaku.CommentsData;
using Gizeta.NuePlayer.Danmaku.ComponentModel;

namespace Gizeta.NuePlayer.Danmaku.Utility
{
    public class CommentManager
    {
        private CommentSpaceManager spaceManager;
        private DispatcherTimer timer;
        private List<UserCommentData> commentList = CommentProvider.Instance.CommentList;
        private int pointer = 0;
        private IPlayer player;
        private TimeSpan oldPlayTime = TimeSpan.Zero;
        
        public CommentManager(Canvas canvas)
        {
            spaceManager = new CommentSpaceManager(canvas);
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(20);
            timer.Tick += timer_Tick;
            player = PlayerMediator.Instance.PlayerContainer.Player;
            player.CurrentStateChanged += player_CurrentStateChanged;
        }

        public void ShowComment(UserCommentData ucd)
        {
            switch (ucd.Mode)
            {
                case CommentMode.Bottom:
                case CommentMode.Top:
                case CommentMode.Scroll:
                case CommentMode.ReverseScroll:
                    BasicComment cmt = new BasicComment(ucd as BasicCommentData);
                    spaceManager.AddComment(cmt);
                    cmt.Completed += cmt_Completed;
                    if (player.CurrentState == MediaElementState.Playing)
                        cmt.Play();
                    break;
            }
        }

        private int seekPointer(TimeSpan time)
        {
            int min = 0, max = commentList.Count - 1, mid;
            while (max - min > 1)
            {
                mid = (min + max) / 2;
                if (commentList[mid].PlayTime == time)
                {
                    return mid;
                }
                else if (commentList[mid].PlayTime < time)
                {
                    min = mid;
                }
                else
                {
                    max = mid;
                }
            }
            if (commentList[max].PlayTime >= time)
            {
                return max;
            }
            else
            {
                return max + 1;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (Math.Abs(player.Position.TotalMilliseconds - oldPlayTime.TotalMilliseconds) >= 100)
            {
                pointer = seekPointer(player.Position);
            }
            oldPlayTime = player.Position;

            if (pointer < commentList.Count)
            {
                while (commentList[pointer].PlayTime <= player.Position)
                {
                    UserCommentData ucd = commentList[pointer];
                    switch (ucd.Mode)
                    {
                        case CommentMode.Bottom:
                        case CommentMode.Top:
                        case CommentMode.Scroll:
                        case CommentMode.ReverseScroll:
                            BasicComment cmt = new BasicComment(ucd as BasicCommentData);
                            spaceManager.AddComment(cmt);
                            cmt.Completed += cmt_Completed;
                            cmt.Play();
                            pointer++;
                            break;

                    }
                    if (pointer >= commentList.Count) break;
                }
            }
        }

        private void cmt_Completed(object sender, EventArgs e)
        {
            spaceManager.RemoveComment(sender as BasicComment);
        }

        private void player_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            switch (player.CurrentState)
            {
                case MediaElementState.Paused:
                case MediaElementState.Stopped:
                case MediaElementState.Buffering:
                    timer.Stop();
                    spaceManager.PauseAllComment();
                    break;
                case MediaElementState.Playing:
                    timer.Start();
                    spaceManager.ResumeAllComment();
                    break;
            }
        }
    }
}
