﻿using System.IO.IsolatedStorage;
using System.Windows.Media;

namespace Gizeta.NuePlayer.Danmaku.Utility
{
    /// <summary>
    /// 弹幕配置类。
    /// </summary>
    public class CommentConfig
    {
        private static readonly CommentConfig instance = new CommentConfig();
        private IsolatedStorageSettings userSettings = IsolatedStorageSettings.ApplicationSettings;

        #region 属性

        private double alpha = 1;
        public double Alpha
        {
            get { return alpha; }
            set { alpha = value; }
        }

        private FontFamily font = new FontFamily("SimHei");
        public FontFamily Font
        {
            get { return font; }
            set { font = value; }
        }

        private bool isBold = true;
        public bool IsBold
        {
            get { return isBold; }
            set { isBold = value; }
        }

        private bool isVisible = true;
        public bool IsVisible
        {
            get { return isVisible; }
            set { isVisible = value; }
        }

        private double fontSizeFactor = 1;
        public double FontSizeFactor
        {
            get { return fontSizeFactor; }
            set { fontSizeFactor = value; }
        }

        #endregion

        private CommentConfig()
        {
        }

        public static CommentConfig Instance
        {
            get { return instance; }
        }

        private void loadSettings()
        {
            bool boldset;
            FontFamily fontset;
            double sizeset, alphaset;
            if (userSettings.TryGetValue<bool>("bold", out boldset))
            {
                isBold = boldset;
            }
            if (userSettings.TryGetValue<FontFamily>("font", out fontset))
            {
                font = fontset;
            }
            if (userSettings.TryGetValue<double>("sizee", out sizeset))
            {
                fontSizeFactor = sizeset;
            }
            if (userSettings.TryGetValue<double>("alpha", out alphaset))
            {
                alpha = alphaset;
            }
        }

        public void SaveSettings()
        {
            if (userSettings.Contains("bold")) userSettings["bold"] = isBold;
            else userSettings.Add("bold", isBold);
            if (userSettings.Contains("font")) userSettings["font"] = font;
            else userSettings.Add("font", font);
            if (userSettings.Contains("sizee")) userSettings["sizee"] = fontSizeFactor;
            else userSettings.Add("sizee", fontSizeFactor);
            if (userSettings.Contains("alpha")) userSettings["alpha"] = alpha;
            else userSettings.Add("alpha", alpha);
        }
    }
}
