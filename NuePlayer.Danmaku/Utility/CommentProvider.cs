﻿using System.Collections.Generic;

using Gizeta.NuePlayer.Danmaku.CommentsData;

namespace Gizeta.NuePlayer.Danmaku.Utility
{
    /// <summary>
    /// 弹幕提供者
    /// </summary>
    public class CommentProvider
    {
        private static readonly CommentProvider instance = new CommentProvider();
        private CommentManager manager;
        private string commentID = "";

        private CommentProvider()
        {
        }

        public static CommentProvider Instance
        {
            get { return instance; }
        }

        public string CommentID
        {
            get { return commentID; }
            set { commentID = value; }
        }

        public CommentManager Manager
        {
            get { return manager; }
            set { manager = value; }
        }

        private List<UserCommentData> commentList = new List<UserCommentData>();
        public List<UserCommentData> CommentList
        {
            get { return commentList; }
        }

        public void Add(UserCommentData data)
        {
            commentList.Add(data);
        }

        public void Clear()
        {
            commentList.Clear();
        }
    }
}
