﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using Gizeta.Macula.Net;

namespace Gizeta.NuePlayer.Danmaku.Utility
{
    /// <summary>
    /// 弹幕发送者
    /// </summary>
    public class CommentSender
    {
        private static readonly CommentSender instance = new CommentSender();

        private CommentSender()
        {
        }

        public static CommentSender Instance
        {
            get { return instance; }
        }

        public void Send(Dictionary<string, string> data)
        {
            string postString =
                string.Format("cid={0}&text={1}&fontsize={2}&color={3}&playtime={4}&timestamp={5}&mode={6}",
                    CommentProvider.Instance.CommentID,
                    data["text"], data["fontsize"], data["color"], data["playtime"], (DateTime.Now.Ticks - DateTime.Parse("1970-01-01 00:00:00").Ticks) / 10000000, data["mode"]);
            string url = "http://localhost/ClientBin/dmpost.php";
            IDownloader downloader = DownloaderFactory.GetDownloader("StringDownloader");
            downloader.Download(new Uri(url + "?" + postString, UriKind.Absolute));
        }
    }
}
