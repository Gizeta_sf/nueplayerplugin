﻿using System.Windows.Controls;

using Gizeta.NuePlayer.Danmaku.Utility;

namespace Gizeta.NuePlayer.Danmaku.Controls
{
    public partial class CommentDataGridControl : UserControl
    {
        public CommentDataGridControl()
        {
            InitializeComponent();
        }

        public void BindingCommentList()
        {
            CommentDataGrid.ItemsSource = CommentProvider.Instance.CommentList;
        }
    }
}
