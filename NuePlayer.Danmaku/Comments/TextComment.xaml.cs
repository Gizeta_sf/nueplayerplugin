﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

using Gizeta.NuePlayer.Danmaku.CommentsData;
using Gizeta.NuePlayer.Danmaku.ComponentModel;
using Gizeta.NuePlayer.Danmaku.Utility;

namespace Gizeta.NuePlayer.Danmaku.Comments
{
    /// <summary>
    /// 文本弹幕实现
    /// </summary>
    public partial class TextComment : UserControl, IComment, IVisual, IText
    {
        public event EventHandler Completed;
        protected Storyboard storyBoard = new Storyboard();
        protected CommentConfig config = CommentConfig.Instance;
        protected TextBlock commentTextBlock = null;

        public TimelineCollection AnimationCollection
        {
            get { return storyBoard.Children; }
        }

        public TimeSpan CurrentTime
        {
            get { return storyBoard.GetCurrentTime(); }
        }

        public Duration LifeTime
        {
            get { return storyBoard.Duration; }
        }

        internal double MeasuredWidth { get; private set; }
        internal double MeasuredHeight { get; private set; }

        public TimeSpan PlayTime { get; private set; }

        public string Text
        {
            get { return commentTextBlock.Text; }
            set { commentTextBlock.Text = value; }
        }

        public Brush TextForeground
        {
            get { return commentTextBlock.Foreground; }
            set { commentTextBlock.Foreground = value; }
        }

        public TextComment()
        {
            InitializeComponent();
            initializeComment(new CommentData());
        }

        public TextComment(CommentData data)
        {
            InitializeComponent();
            initializeComment(data);
        }

        protected virtual void initializeComment(CommentData data)
        {
            this.RenderTransform = new CompositeTransform();
            this.Projection = new PlaneProjection();
            commentTextBlock = CommentTextBlock;
            this.FontWeight = config.IsBold ? FontWeights.Bold : FontWeights.Normal;
            this.FontFamily = config.Font;
            this.Opacity = config.Alpha;
            storyBoard.Duration = data.LifeTime;
            this.PlayTime = data.PlayTime;
        }

        public virtual void Pause()
        {
            storyBoard.Pause();
        }

        public virtual void Play()
        {
            storyBoard.Completed += storyBoard_Completed;
            storyBoard.Begin();
        }

        public virtual void Resume()
        {
            storyBoard.Resume();
        }

        internal void SetMeasureSize()
        {
            this.MeasuredWidth = this.DesiredSize.Width;
            this.MeasuredHeight = this.DesiredSize.Height;
        }

        protected virtual void storyBoard_Completed(object sender, EventArgs e)
        {
            if (this.Completed != null)
            {
                this.Completed(this, null);
            }
            storyBoard.Completed -= storyBoard_Completed;
        }
    }
}
