﻿using System;
using System.Windows.Media;

using Gizeta.NuePlayer.Danmaku.CommentsData;
using Gizeta.NuePlayer.Danmaku.ComponentModel;

namespace Gizeta.NuePlayer.Danmaku.Comments
{
    /// <summary>
    /// 普通弹幕实现
    /// </summary>
    public class BasicComment : TextComment, IUser
    {
        public uint ID { get; private set; }
        public bool IsLocked { get; private set; }
        internal int LayerIndex { get; set; }
        public CommentMode Mode { get; private set; }
        internal double Speed { get; set; }
        public DateTime Timestamp { get; private set; }
        public string UserID { get; private set; }

        internal double Bottom { get; private set; }
        
        internal double RotationX
        {
            get { return (this.Projection as PlaneProjection).RotationX; }
            set { (this.Projection as PlaneProjection).RotationX = value; }
        }

        internal double RotationY
        {
            get { return (this.Projection as PlaneProjection).RotationY; }
            set { (this.Projection as PlaneProjection).RotationY = value; }
        }

        internal double RotationZ
        {
            get { return (this.Projection as PlaneProjection).RotationZ; }
            set { (this.Projection as PlaneProjection).RotationZ = value; }
        }

        internal double X
        {
            get { return (this.RenderTransform as CompositeTransform).TranslateX; }
            set { (this.RenderTransform as CompositeTransform).TranslateX = value; }
        }

        internal double Y
        {
            get { return (this.RenderTransform as CompositeTransform).TranslateY; }
            set
            {
                (this.RenderTransform as CompositeTransform).TranslateY = value;
                this.Bottom = value + this.MeasuredHeight;
            }
        }

        public BasicComment(BasicCommentData data)
            : base(data)
        {
            initializeComment(data);
        }

        protected virtual void initializeComment(BasicCommentData data)
        {
            this.ID = data.ID;
            this.IsLocked = data.IsLocked;
            this.Mode = data.Mode;
            this.Timestamp = data.Timestamp;
            this.UserID = data.UserID;
            
            this.TextForeground = new SolidColorBrush(data.Color);
            this.FontSize = data.FontSize * config.FontSizeFactor;            
            this.Text = data.Text;
        }
    }
}
