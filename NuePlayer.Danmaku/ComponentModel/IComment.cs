﻿using System;
using System.Windows;

namespace Gizeta.NuePlayer.Danmaku.ComponentModel
{
    /// <summary>
    /// 定义弹幕所实现的方法和属性。
    /// </summary>
    public interface IComment
    {
        event EventHandler Completed;

        Duration LifeTime { get; }
        TimeSpan PlayTime { get; }

        void Pause();
        void Play();
        void Resume();
    }
}
