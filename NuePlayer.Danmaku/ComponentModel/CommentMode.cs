﻿namespace Gizeta.NuePlayer.Danmaku.ComponentModel
{
    /// <summary>
    /// 指定弹幕模式。
    /// </summary>
    public enum CommentMode
    {
        Scroll,
        Bottom,
        Top,
        ReverseScroll,
        Advanced,
        Script,
        Zoome
    }
}
