﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;

namespace Gizeta.NuePlayer.Danmaku.ComponentModel
{
    /// <summary>
    /// 定义可视化弹幕所实现的方法和属性。
    /// </summary>
    public interface IVisual
    {
        double ActualHeight { get; }
        double ActualWidth { get; }
        Geometry Clip { get; set; }
        Cursor Cursor { get; set; }
        Size DesiredSize { get; }
        Effect Effect { get; set; }
        double Height { get; set; }
        Thickness Margin { get; set; }
        double MaxHeight { get; set; }
        double MaxWidth { get; set; }
        double MinHeight { get; set; }
        double MinWidth { get; set; }
        double Opacity { get; set; }
        Brush OpacityMask { get; set; }
        DependencyObject Parent { get; }
        Projection Projection { get; set; }
        Size RenderSize { get; }
        Transform RenderTransform { get; set; }
        Point RenderTransformOrigin { get; set; }
        Visibility Visibility { get; set; }
        double Width { get; set; }

        TimelineCollection AnimationCollection { get; }
    }
}
