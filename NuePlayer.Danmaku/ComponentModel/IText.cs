﻿using System;
using System.Windows;
using System.Windows.Media;

namespace Gizeta.NuePlayer.Danmaku.ComponentModel
{
    /// <summary>
    /// 定义文本弹幕所实现的方法和属性。
    /// </summary>
    public interface IText
    {
        Brush Background { get; set; }
        Brush BorderBrush { get; set; }
        Thickness BorderThickness { get; set; }
        FontFamily FontFamily { get; set; }
        double FontSize { get; set; }
        FontStretch FontStretch { get; set; }
        FontStyle FontStyle { get; set; }
        FontWeight FontWeight { get; set; }
        Brush Foreground { get; set; }
        Thickness Padding { get; set; }

        TimeSpan CurrentTime { get; } 
        string Text { get; set; }
        Brush TextForeground { get; set; }
    }
}
