﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Gizeta.NuePlayer.Danmaku.ComponentModel
{
    /// <summary>
    /// 定义弹幕数据解析实现的方法。
    /// </summary>
    public interface IDataParser
    {
        event EventHandler ParseCompleted;
        event EventHandler<ApplicationUnhandledExceptionEventArgs> Erred;

        void Load(Dictionary<string,string> param);
    }
}
