﻿using System;

namespace Gizeta.NuePlayer.Danmaku.ComponentModel
{
    /// <summary>
    /// 定义用户弹幕所实现的方法和属性。
    /// </summary>
    public interface IUser
    {
        uint ID { get; }
        bool IsLocked { get; }
        CommentMode Mode { get; }
        DateTime Timestamp { get; }
        string UserID { get; }
    }
}
