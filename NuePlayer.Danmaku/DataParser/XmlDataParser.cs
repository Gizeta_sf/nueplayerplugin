﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml.Linq;

using Gizeta.Macula.Net;
using Gizeta.NuePlayer.Danmaku.CommentsData;
using Gizeta.NuePlayer.Danmaku.ComponentModel;
using Gizeta.NuePlayer.Danmaku.Utility;

namespace Gizeta.NuePlayer.Danmaku.DataParser
{
    /// <summary>
    /// XML弹幕数据解析者
    /// </summary>
    public class HttpXmlDataParser : IDataParser
    {
        public event EventHandler ParseCompleted;
        public event EventHandler<ApplicationUnhandledExceptionEventArgs> Erred;

        private CommentProvider provider = CommentProvider.Instance;

        public void Load(Dictionary<string,string> param)
        {
            string cid;
            if (!param.TryGetValue("cid", out cid))
            {
                if (this.Erred != null)
                {
                    this.Erred(this, new ApplicationUnhandledExceptionEventArgs(new Exception("弹幕地址为空"), false));
                }
                return;
            }
            provider.CommentID = cid;
            Uri url = cid.Contains("://") ? new Uri(cid, UriKind.Absolute) : new Uri(cid, UriKind.Relative);
            IDownloader downloader = DownloaderFactory.GetDownloader("StreamDownloader");
            downloader.DownloadCompleted += downloader_DownloadCompleted;
            downloader.Erred += downloader_Erred;
            downloader.Download(url);
        }

        private void parseXml(XDocument xmlDoc)
        {
            XElement xml = xmlDoc.Element("dm");
            if (xml == null)
            {
                return;
            }
            var dmArr = from dm in xml.Elements("i")
                        where dm.Value != ""
                        where dm.Attribute("p") != null
                        select dm;
            foreach (var dm in dmArr)
            {
                Dictionary<string, object> data = parseElementData(dm);
                provider.Add(new BasicCommentData(data));
            }
            provider.CommentList.Sort((d1, d2) => d1.PlayTime.CompareTo(d2.PlayTime));
            if (this.ParseCompleted != null)
            {
                this.ParseCompleted(this, null);
            }
        }

        private Dictionary<string, object> parseElementData(XElement source)
        {
            Dictionary<string, object> data = new Dictionary<string, object>();
            data["text"] = source.Value.Replace("\\n","\r\n");
            string attr = source.Attribute("p").Value;
            string[] attrData = attr.Split(',');
            data["playTime"] = double.Parse(attrData[0]);
            switch (attrData[1])
            {
                case "1":
                    data["mode"] = CommentMode.Scroll;
                    break;
                case "4":
                    data["mode"] = CommentMode.Bottom;
                    break;
                case "5":
                    data["mode"] = CommentMode.Top;
                    break;
                case "6":
                    data["mode"] = CommentMode.ReverseScroll;
                    break;
            }
            data["lifeTime"] = 3.0;
            data["fontsize"] = double.Parse(attrData[2]);
            data["color"] = int.Parse(attrData[3]);
            return data;
        }

        private void downloader_Erred(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (this.Erred != null)
            {
                this.Erred(sender, e);
            }
        }

        private void downloader_DownloadCompleted(object sender, DownloadCompletedEventArgs e)
        {
            parseXml(XDocument.Load((Stream)e.Result));
        }
    }
}
