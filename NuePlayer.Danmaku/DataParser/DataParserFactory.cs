﻿using System;
using System.Collections.Generic;

using Gizeta.NuePlayer.Danmaku.ComponentModel;

namespace Gizeta.NuePlayer.Danmaku.DataParser
{
    /// <summary>
    /// 弹幕数据解析工具工厂类
    /// </summary>
    public class DataParserFactory
    {
        private static Dictionary<string, Type> dataParserTypeList = new Dictionary<string, Type>() { { "http", typeof(HttpXmlDataParser) } };

        public static void RegisterDataParser(string name, Type type)
        {
            if (typeof(IDataParser).IsAssignableFrom(type))
            {
                dataParserTypeList[name] = type;
            }
        }

        public static IDataParser GetDataParser(string name)
        {
            if (dataParserTypeList.ContainsKey(name))
            {
                return Activator.CreateInstance(dataParserTypeList[name]) as IDataParser;
            }
            else
            {
                return null;
            }
        }
    }
}
